#!/bin/sh

# Terminate already running bar instances
killall -q polybar
while pgrep -x polybar >/dev/null; do sleep 1; done


polybar left &
polybar center &
polybar right &

