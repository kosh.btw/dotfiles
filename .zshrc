# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
ZSH="~/.config/.oh_my_zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="cloud"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	git
        zsh-autosuggestions
        zsh-syntax-highlighting
        colored-man-pages
        history-substring-search
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"



# ---- INITIALISATION ----
neofetch --ascii_colors 5 5 5 --block_range  5 5 --block_width 20 --block_height 2

# ---- VARIABLES ----
school_project="/home/koshin-s-hegde/My-Folder/Koshin-S-Hegde/Koshin-Special/Programs/Python/School-Project-trial-1"
my_divice=d8925c9d6b6a7ce6
trash=/home/koshin-s-hegde/.local/share/Trash/files/
programs=/home/koshin-s-hegde/My-Folder/Koshin-S-Hegde/Koshin-Special/Programs


# ---- Fucntoins AND ALIASES ---- 
alias refresh="source $ZDOTDIR/.zshrc"
alias grep="grep -i"
alias clear="clear; refresh"
alias show-file-size="du -sh "
alias mocp="mocp -T ~/.config/moc/theme"
alias read_eleven_minutes="nvim -R ~/.eleven_minutes.txt"
alias rm="trash-put"

build_and_run_cpp() {
	mkdir -p .builds/
	g++ $@ -o .builds/auto_build_cpp -g
	./".builds/auto_build_cpp"
}

build_cpp() {
	mkdir -p .builds/
	g++ $@ -o .builds/auto_build_cpp -g
}

build_and_run_asm() {
    mkdir -p .builds/
    nasm -f elf -g -Fdwarf $@ -o .builds/auto_build_asm.o
    ld -m elf_i386 .builds/auto_build_asm.o -o .builds/auto_build_asm
    ./".builds/auto_build_asm"
    echo -e "\nExit code:- $?"
}

cpi() {
	IFS="\/"
	read -rA sliced <<< "$1"
	pv "$1" > "${2}${sliced[-1]}"
}

clipboard-copy() {
	echo "$1" | xclip -selection clipboard
}

