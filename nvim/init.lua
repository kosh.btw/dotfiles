require "kosh.options"
require "kosh.keymaps"
require "kosh.plugins"


-- Fold
require("fold-preview").setup()
require("pretty-fold").setup({
    sections = {
        left = {
            'content',
        },
        right = {
            ' ', 'number_of_folded_lines',
            function(config) return config.fill_char:rep(3) end
        }
    },
    fill_char = " "
})


-- Scratchpad setup
file_path = vim.uri_to_fname(vim.uri_from_bufnr(0))

if (file_path == "/home/koshin-s-hegde/.backup/notes.txt") then
    vim.api.nvim_set_keymap("n", "ZZ", ":!i3-msg \\[instance='notes'\\] move scratchpad<CR><CR>", {noremap = true})
end


-- MISC
vim.cmd "colorscheme onedarker"

require("lualine").setup()

