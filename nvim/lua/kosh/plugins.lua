return require("packer").startup(function(use)
    use { 'anuvyklack/fold-preview.nvim', requires = 'anuvyklack/keymap-amend.nvim'}
    use "anuvyklack/pretty-fold.nvim"

    use {"neoclide/coc.nvim", branch = "release"}

    use {
        "nvim-lualine/lualine.nvim",
        requires = { "kyazdani42/nvim-web-devicons", opt = true }
    }

    use "LunarVim/Onedarker.nvim"

    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.2',
        requires = { {'nvim-lua/plenary.nvim'}, {"nvim-tree/nvim-web-devicons"} }
    }
end)

