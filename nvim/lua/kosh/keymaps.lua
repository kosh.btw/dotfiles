local set_keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
vim.g.mapleader = " "


set_keymap("n", "<C-s>", ":w<CR>", opts)  -- Save

-- Autocomplete brackets and quotes
set_keymap("i", "\"", "\"\"<left>", opts)
set_keymap("i", "'", "''<left>", opts)
set_keymap("i", "(", "()<left>", opts)
set_keymap("i", "[", "[]<left>", opts)
set_keymap("i", "{", "{}<left>", opts)

-- Coc config
vim.cmd[[ inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>" ]]

-- Split Window config
set_keymap("n", "<C-h>", "<C-w>h", opts)
set_keymap("n", "<C-j>", "<C-w>j", opts)
set_keymap("n", "<C-k>", "<C-w>k", opts)
set_keymap("n", "<C-l>", "<C-w>l", opts)

-- Tabs
set_keymap("n", "<S-l>", ":tabn<CR>", opts)
set_keymap("n", "<S-h>", ":tabp<CR>", opts)

-- NOH
set_keymap("n", "<leader>n", ":noh<CR>", opts)

-- Telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})

-- Indent
set_keymap("n", "<leader>=", "gg=G", opts)

