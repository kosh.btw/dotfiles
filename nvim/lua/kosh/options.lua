-- Numbers
vim.opt.relativenumber = true			-- Relative numbers
vim.opt.number = true                   -- Normal numbers

-- Tabs
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.expandtab = true			    -- Converts tabs to spaces

vim.opt.cb = unnamedplus				-- Clipboard
vim.opt.mouse = "a"						-- Mouse enable
vim.opt.fileencoding = "utf-8"			-- File encoding
vim.opt.showtabline = 2					-- The top tabs
vim.opt.cursorline = true               -- Underline
vim.opt.termguicolors = true
vim.opt.undofile = true                 -- UNLIMITED UNDOS!!!

-- Folds
vim.opt.foldmethod = "expr"
vim.cmd([[
    set foldexpr=GetPotionFold(v:lnum)
    let level = 0

    function! GetPotionFold(lnum)
        if getline(a:lnum + 1) =~? "\{"
            let g:level += 1
        elseif getline(a:lnum - 1) =~? "\}"
            echo "a"
            let g:level -= 1
        endif
        return g:level
    endfunction
]])

vim.opt.foldlevel = 0

