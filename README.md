## Intro
Hey there!!!
This is my first attempt on a rice...

## What are the config files mainly for
- [i3-gaps](https://github.com/Airblader/i3)
- [moc](http://moc.daper.net/)
- [neofetch](https://github.com/dylanaraps/neofetch)
- [polybar](https://polybar.github.io/)
- [i3-blocks](https://vivien.github.io/i3blocks/)

## Screenshots
![alt text](ScreenShots/Plain.png "Plain")
![alt text](ScreenShots/Music.png "Music")
![alt text](ScreenShots/Terminal.png "Terminal")
